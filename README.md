## IBMi Toolkit (iit) for Ruby

## Summary
The goal of the IBMi Toolkit for Ruby is to make IBM i resources more easily digestible from Ruby without having to write a lot of code.  For example, checking to see if a user exists should be a simple API call (i.e. IIT.user_exists? "QSECOFR").  As noted in the **Need Help** section below this project has eventual goals of utilizing the xmlservice Gem on youngiprofessionals.com/wiki

It should be noted that this project is highly likely to change over the coming months (i.e. renaming/relocating of methods) so please know that as you adopt.

### Installation

``` ruby
## Gemfile 
gem 'iit'
```

``` ruby
## gem install
gem install iit
```

## Basic use

``` ruby

## Check if an IBM i user exists
if IIT.user_exists? "QSECOFR"
  # Do something
end

# Run SQL statements
IIT.run_sql "DELETE * FROM MYTABLE"

# Obtain the value from a *DTAARA
current_nbr = IIT.DSPDTAARA('MYLIB','MYDTAARA')[:value].to_i

```

## Need Help
* Incorporate XMLSERVICE capabilities as an optional dependency. XMLSERVICE has some very nice facilities for stateful jobs.
* Unit tests need some help.  RSpec is the testing framework.  Fork and pull to add some more.

Have fun!